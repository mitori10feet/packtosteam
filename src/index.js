








function init () {
    const shell = require('shelljs');
    const fs = require('fs');
    
    let isMZ =  document.getElementById('rm_version').value == 'MZ' ? true : false;
    let steam_app_id = document.getElementById('steam_app_id').value;
    if (steam_app_id) steam_app_id = Number(steam_app_id);
    /** 輸出資料夾 */
    shell.exec(`rm -rf ./dist/`);
    shell.exec(`mkdir ./dist`);

    // copy steam api 到原始專案
    shell.exec(`rm -rf ./origin/lib`);    
    shell.exec(`cp -R ./lib ./origin/lib`, { async: false})

    // 複製greenworks
    shell.exec(`rm -rf origin/greenworks`);    
    shell.exec(`cp ./template/scripts/greenworks.js ./origin/greenworks.js`)
    if (isMZ) {        
        /** MZ 覆蓋main */
        shell.exec(`cp ./template/scripts/main.js ./origin/js/main.js`, { async: false})
    }


    /** 複製 建立steam_appid */
    shell.exec(`rm -rf ./origin/steam_appid.txt`);   
    fs.writeFileSync('./origin/steam_appid.txt', steam_app_id +"");

    return;        


}

function startPack () {
    // return;
    const shell = require('shelljs');
    let makeWindow = document.getElementById('pack_win').checked;
    let makeMac = document.getElementById('pack_mac').checked;
    let makeLinux = document.getElementById('pack_linux').checked;;

    let exeName = 'Game'
    console.error(`makeWindow ${makeWindow}`)
    /** 建立mac專案 */
    if (makeWindow) {
        shell.exec(`mkdir ./dist/window`);
        shell.exec(`cp -R template/nwjs-v0.71.0-win-x64/* ./dist/window`, { async: false});
        
        shell.exec(`cp -R ./origin/* ./dist/window`, { async: false})
        shell.exec(`mv dist/window/nw.exe ./dist/window/${exeName}.exe`);
    }

    /** 建立linux */
    if (makeLinux) {

        shell.exec(`mkdir ./dist/linux`);
        shell.exec(`cp -R ./template/nwjs-v0.71.0-linux-x64/* ./dist/linux`, { async: false});
        shell.exec(`cp -R ./origin/* ./dist/linux`, { async: false})
        shell.exec(`mv ./dist/linux/nw ./dist/linux/${exeName}`);        
    }
    if (makeMac) {
        shell.exec(`mkdir ./dist/mac`);
        shell.exec(`cp -R  ./template/nwjs-v0.71.0-osx-x64/nwjs.app ./dist/mac/nwjs-v0.71.0-osx-x64`, { async: false});
        shell.exec(`cp -R ./origin/* ./dist/mac/nwjs-v0.71.0-osx-x64/Contents/Resources/app.nw`, { async: false})
        shell.exec(`mv ./dist/mac/nwjs-v0.71.0-osx-x64 ./dist/mac/${exeName}.app`);                
    }
    document.getElementById('loading').setAttribute('style', 'opacity:0');
    alert(`done`)
    
}



function pack () {
    document.getElementById('loading').setAttribute('style', 'opacity:1');
    setTimeout(() => {
        init();
        startPack();   
    }, 200)






}

