


# 配置

template 資料夾放
```
nwjs-v0.71.0-linux-x64
nwjs-v0.71.0-win-ia32
nwjs-v0.71.0-win-x64
nwjs-v0.71.0-mac
```

# 使用流程
    - 從rpgmaker 輸出html 專案，複製 www裡面的內容到origin資料夾中
    - [MV限定追加流程請點我](#MV限定流程)
    - 複製rpgmaker底下的package.json 到origin專案中
```shell
    此時目錄結構如下
+--origin
|    +-- audio
|    +-- css
|    +-- data
|    +-- effects
|    +-- fonts
|    +-- icon
|    +-- img
|    +-- index.html
|    +-- js
|    +-- package.json
```
    - 勾選要打包的版本
    - 點選打包
    - 完成


# MV限定流程
在index.hmtl 中找到 下面這行並新增一行
這是新增前
```html
<link rel="stylesheet" type="text/css" href="fonts/gamefont.css">
```

這是新增後前
```html
<link rel="stylesheet" type="text/css" href="fonts/gamefont.css">
<script type="text/javascript" src="greenworks.js"></script>
```




